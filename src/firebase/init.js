import firebase from 'firebase'
import firestore from 'firebase/firestore'
const firebaseConfig = {
    apiKey: "AIzaSyCbLI2rVf-DR2GLX4CvZgNgKSKJ4cOf0pU",
    authDomain: "webapp1-c344a.firebaseapp.com",
    databaseURL: "https://webapp1-c344a.firebaseio.com",
    projectId: "webapp1-c344a",
    storageBucket: "webapp1-c344a.appspot.com",
    messagingSenderId: "291910534061",
    appId: "1:291910534061:web:82c0a4c668263e3cff5724",
    measurementId: "G-43LMRV8ZV3"
    };  
  const firebaseApp = firebase.initializeApp(firebaseConfig);
  export default firebaseApp.firestore();



