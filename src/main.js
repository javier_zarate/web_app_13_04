
import Vue from 'vue'
import App from './App.vue'
import router from './router/index'
import BootstrapVue from 'bootstrap-vue'

import firebase from 'firebase'

import 'bootstrap/dist/css/bootstrap.css'
import 'bootstrap-vue/dist/bootstrap-vue.css'
import AuthService from 'msal'
Vue.config.productionTip = false



Vue.use(BootstrapVue);
//Vue.prototype.$authService = new AuthService();

/*const app = null

firebase.auth().onAuthStateChanged(() =>{
    if(!app){
      new Vue({
        router,
        render: h => h(App)
      }).$mount('#app')
    }
  }
)
*/

new Vue({
  router,
  render: h => h(App)
  }).$mount('#app')



