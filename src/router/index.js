import Vue from 'vue'
import VueRouter from 'vue-router'
import AuthService from "./../ad_auth/authad"
import firebase from 'firebase'
import Navigation from "../components/layout/Navigation.vue"

var userlogged = null;

Vue.use(VueRouter)


const router = new VueRouter({
  mode: 'history',
  routes: [
  {
    path: '/',
    name: 'Home',
    component: () => import(/* webpackChunkName: "about" */ '../views/Home.vue')
  },
  {
    path: '/issue',
    name: 'Issue',
    component: () => import(/* webpackChunkName: "about" */ '../views/Issue.vue')
  },
  {
    path: '/report',
    name: 'Report',
    component: () => import(/* webpackChunkName: "about" */ '../views/Report.vue')
  
    ,meta: {
      requiresAuth: true
    }
  
  }
  ]
  /*,
  created() {
    this.authService = new AuthService();
  },
  methods: {
    getlogged() {
    alert("looking user logged");
    return this.authService.getlogged();

    }
  }
  */
});


router.beforeEach((to, from, next) =>{
  if(to.matched.some(record =>record.meta.requiresAuth)){
    const authService = new AuthService();
    userlogged = authService.getlogged();
    if(!userlogged){
      next({
        name: 'Home'
      })
    }
    else{
      next();
    }
    
  }
  else{
    next();
  }
})



export default router

